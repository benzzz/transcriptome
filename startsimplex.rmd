---
title: "B. simplex"
author: "Rotem Hadar"
date: "16 Shvat 5779"
output:
  html_notebook: default
  pdf_document: default
  word_document: default
---
1. Load libraries
```{r, warning = F, message = F, echo = F}
library(DESeq2)
library(formattable)
library(tidyverse)
library(ggplot2)
library(gridExtra)
library(plotly)
library(kableExtra)
library(xlsx)
library(pheatmap)
library("vsn")
library("RColorBrewer")
library("apeglm")
```

2. Read files
```{r, warning = F, message = F, echo = F}
all.filenames = list.files("data/5_counts", full.names = T)
pat = "[VD]B"
toread = all.filenames[grep(x = all.filenames, pattern = pat, invert = F)]
```
3. Helper function to read files
```{r, warning = F, message = F, echo = F}
# nead to enter the filder and the file suffix to get right names
readfile = function(file.name) {
  read.csv(file.name, sep = "\t", header = F) -> x
  temp.name = gsub(pattern = "data/5_counts/", replacement = "", x = file.name)
  name = gsub(pattern = "_counts.txt", replacement = "", x = temp.name)
  names(x) = c("gene", name)
  return(x)
}
```
4. Read all files and arrange the data
```{r, warning = F, message = F, echo = F}
data = lapply(toread, readfile)
combined = bind_cols(data[1:7])
# TRY
combined = read.csv("data/simplex/tsviya.tsv")
gene = combined[,1]
combined = combined %>% select(-starts_with("ge"))

combined = cbind(gene, combined)

## Take last line with general feature of alignment
summ.table = combined[grep(x = combined[,1], pattern = "__"),]
combined = combined[grep(x = combined[,1], pattern = "__", invert = T),]
```
5. Manually correction for problematic genes
```{r, warning = F, message = F, echo = F}
```
```{r, warning = F, message = F, echo = F}
# Gene names as rows
row.names(combined)=combined$gene
# row.names(combined)=combined$X
```
### Plot: reads without faetures
```{r, warning = F, message = F, echo = F}
# nfp = summ.table[1,3:8] / (apply(combined[,3:8], 2, sum) + summ.table[1,3:8])
# barplot(as.numeric(nfp)*100, names.arg = names(nfp))
# title(main = "Reads without feature", y = "%")
```
### Plot: reads not aligned
```{r, warning = F, message = F, echo = F}
# nar = summ.table[4,3:8] / (apply(combined[,3:8], 2, sum) + summ.table[1,3:8])
# barplot(as.numeric(nar)*100, names.arg = names(nar))
# title(main = "Reads not aligned", y = "%")
```
6. Make DDS
```{r, warning = F, message = F, echo = F}
cts = as.matrix(combined[,2:length(combined)])

coldata = data.frame(x = str_sub(colnames(cts), start = 1, end = -2), 
                     row.names = colnames(cts))
colnames(coldata)="sample"

dds = DESeqDataSetFromMatrix(countData = cts, colData = coldata, design = ~sample)
rld = rlog(dds, blind=TRUE)

```
7. Average samples
```{r, warning = F, message = F, echo = F}
# need to change names
avg = combined %>% mutate(
average.bacteria = apply(combined[,grep(names(combined), pattern = "D")], 1, mean)
) %>% mutate(
average.vesicles = apply(combined[,grep(names(combined), pattern = "V")], 1, mean)
)
avg.columns = seq(  from = length(avg) - length(unique(coldata$sample)) + 1
                    , to = length(avg))
avg = avg[,c(1,avg.columns)]
# avg %>% arrange(desc(average.bacteria))


ggplot(avg, aes(x = log(average.bacteria), y = log(average.vesicles))) + 
  geom_point(alpha = 0.1) + 
  theme_classic()

sum(avg$average.bacteria > avg$average.vesicles)
sum(avg$average.bacteria < avg$average.vesicles)

```
8. Pre-filtering
```{r Pre-filtering, warning = F, message = F, echo = F}
keep <- rowSums(counts(dds)) >= 10
dds <- dds[keep,]
dds$sample = factor(dds$sample, levels = c("DB", "VB"))
```
9. Compute DESeq2
```{r, warning = F, message = F, echo = F}
dds <- DESeq(dds)
res <- results(dds)

shr = lfcShrink(dds, coef = resultsNames(dds)[2], type="apeglm")
res = lfcShrink(dds, coef = resultsNames(dds)[2], type="apeglm")
```

```{r temp, include = F}
ntd <- normTransform(dds)
# meanSdPlot(assay(ntd))
vsd <- vst(dds, blind=FALSE)
# meanSdPlot(assay(vsd))


select <- order(rowMeans(counts(dds,normalized=TRUE)),
                decreasing=TRUE)[1:20]
df <- as.data.frame(colData(dds))
sampleDists <- dist(t(assay(vsd)))

sampleDistMatrix <- as.matrix(sampleDists)
rownames(sampleDistMatrix) <- paste(vsd$sample, 1:3, sep="-")
colnames(sampleDistMatrix) <- NULL
colors <- colorRampPalette( rev(brewer.pal(9, "Blues")) )(255)
pheatmap(sampleDistMatrix,
         clustering_distance_rows=sampleDists,
         clustering_distance_cols=sampleDists,
         col=colors)
plotPCA(vsd, intgroup=c("sample")) + theme_classic()

rld = as.matrix(cts)

heatmap(as.matrix(cts), labRow = F)
```
10. Combine tables, add links
```{r, warning = F, message = F, echo = F}
# PROBLEM LINK NOT READY YET!!!!
# preurl = "https://www.ncbi.nlm.nih.gov/gene?term=("
# sufurl = "[gene])%20AND%20(Bacillus%20simplex%20str.%20K-12%20substr.%20MG1655[orgn])%20AND%20alive[prop]%20NOT%20newentry[gene]&sort=weight"

all_table = res %>% data.frame() %>% 
rownames_to_column() %>% 
rename(gene = rowname)


all_table = merge(avg, all_table)
# all_table$links = paste0(preurl, all_table$gene, sufurl)
genedescriptions = read.csv("data/simplex/gffcodes.csv")
names(genedescriptions) = c("X", "gene", "description")
geneswp = read.csv("data/simplex/gffwpcodes.csv")
names(geneswp) = c("x", "gene", "wp")
genedescriptions$gene = as.character(genedescriptions$gene)
all_table = left_join(all_table,geneswp, genedescriptions, by = "gene")
```
### Histograns
```{r}
hist(log(avg$average.bacteria))
hist(log(avg$average.vesicles))
```
### Filtering
#### parameters:
* average.bacteria > 20 Or
* average.vesicles > 20
* padj (p value corrected to false discovery rate) < 0.05
```{r}
filtered = all_table %>% 
  filter(average.bacteria > 20 |
           average.vesicles > 20
         , padj < 0.05)

```


### Plot: high and low FC
```{r, warning = F, message = F, echo = F}
toplot1 = data.frame(filtered) %>% 
  arrange(log2FoldChange) %>% 
  slice(1:20)
ggplot(toplot1, aes(x = gene, y = log2FoldChange, error = lfcSE)) +
  geom_col() + 
  ggtitle("Lowest foldchange abundance in vesicles") + 
  theme_classic()
```
```{r, warning = F, message = F, echo = F}
toplot2 = data.frame(filtered) %>%
  arrange(desc(log2FoldChange)) 
toplot2 = toplot2 %>% top_n(20, log2FoldChange)
ggplot(toplot2, aes(x = gene, y = log2FoldChange, error = lfcSE)) +
  geom_col() + 
  ggtitle("Highest foldchange abundance in vesicles") + 
  theme_classic()

```
```{r}
to_write = filtered %>% filter(log2FoldChange > 2) %>% select(gene)
write.csv(file = "results/simplex_genes_abv_FC2.csv", to_write)
```

### Plot: highly expressed genes
```{r, warning = F, message = F, echo = F}
toplot3 = filtered %>%
  arrange(desc(average.bacteria)) 
toplot3 = toplot3 %>% top_n(20, average.bacteria) %>% 
  select(gene, average.bacteria, average.vesicles) %>% 
  gather(value = average.expression, sample, 2:3)
toplot3$sample[toplot3$sample == "average.bacteria"] = "B. simplex"
toplot3$sample[toplot3$sample == "average.vesicles"] = "vesicles"


g1 = ggplot(toplot3, aes(x = reorder(gene, seq_along(gene), width = 500)
                        , fill = sample
                        , y = average.expression)) + 
  geom_col(position="dodge") +
  scale_y_continuous(expand = c(0,0)) +
  labs(x = "genes", title = "Top 20 expressed genes", y = "average expression", fill = "")+
  theme_classic() +
  theme( axis.text.x = element_text(angle = 270, hjust = 0, vjust = 0)
        , text = element_text(size=20)
        , plot.title = element_text(hjust = 0.5)
        , legend.position = c(0.8, 0.8))
ggplotly(g1, width = 800)
# g1
```



```{r, warning = F, message = F, echo = F, include = F}
long = 700
percentage = vector(length = long)
for (i in seq_along(percentage)){
  take = i
  top100bac = unlist(as.list(filtered %>% arrange(desc(average.bacteria)) %>% slice(1:take) %>% select(gene)))
  top100ves = unlist(as.list(filtered %>% arrange(desc(average.vesicles)) %>% slice(1:take) %>% select(gene)))
  percentage[i] = length(intersect(top100bac, top100ves))/take
}
plot(percentage[20:700], xlab = "top N genes [20:700]", ylab = "ratio")
title("percentage of genes common in vesicles from the common genes in bacteria")


mean(percentage)
```


11. Find genes are in top N in cells and not in vesicles and vice versa.
<p><span style="color:green">In green, genes highly expressed in vesicles but not in cells</span>.</p>
<p><span style="color:red">In red, genes highly expressed in cells but not in the vesicles</span>.</p>
```{r, warning = F, message = F, echo = F, results="asis"}

# take only top list
fifty = 50
top50bac = filtered %>% arrange(desc(average.bacteria)) %>% slice(1:fifty)
top50ves = filtered %>% arrange(desc(average.vesicles)) %>% slice(1:fifty)

high_in_bacteria_not_in_ves = setdiff(top50bac$gene, top50ves$gene)
high_in_vesicles_not_in_bac = setdiff(top50ves$gene, top50bac$gene)

library(knitr)
knit_print(paste("<span style='color:green'>", high_in_vesicles_not_in_bac,"</span>"))
knit_print(paste("<span style='color:red'>", high_in_vesicles_not_in_bac,"</span>"))
```

## Genes highly expressed 
```{r}
high_FC = unlist(as.list(filtered %>% filter(log2FoldChange > 2) %>% select(gene)))
low_FC = unlist(as.list(filtered %>% filter(log2FoldChange < 0) %>% select(gene)))
```

```{r, warning = F, message = F, echo = F}
niceTable = filtered %>%
data.frame() %>%
arrange(desc(baseMean)) %>% 
mutate(link = 1:n()) %>% 
mutate(link = cell_spec(link, link = links, bold = T, color = "#0000FF", underline = T)) %>% 
select(-links) %>% 
slice(1:(fifty))


niceTable %>%
kable(escape = F, align = "c", digits = 3) %>%
kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive")) %>%
row_spec(match(low_FC, niceTable$gene ,nomatch = 0)
         , background = "#D7261E", color = "white") %>%
row_spec(match(high_FC, niceTable$gene ,nomatch = 0)
         , background = "green", color = "white") %>%
row_spec(0, background = "grey") %>% 
  column_spec(10, color = "#0000FF", underline = T)

```


### <span style="color:orange">From previous experiment, partial</span>
```{r, warning = F, message = F, echo = F}
# coli = read.xlsx("C:/Users/rotemh.WISMAIN/Google Drive/@מגדל השן/vesicles/@78/e_coli.xlsx",1)
# genes_from_previous_experiment = c("pgk", "rpoA", "cysJ", "sgbU", "atpA", "metL", "lacZ", "yqeB", "bdcR","fusA","hybO", "ftsK", "glnD", "aceA", "potB", "yeiA","ssrA", "rnpA", "preA")
# 
# as_old_table = all_table[match(genes_from_previous_experiment, all_table$gene, nomatch = 0),] %>% 
# filter(average.bacteria > 20) %>% 
# arrange(desc(average.bacteria)) %>% 
# mutate(link = 1:n()) %>% 
# mutate(link = cell_spec(link, link = links, bold = T, color = "#0000FF", underline = T)) %>% 
# select(-links) 
# as_old_table %>%   kable(escape = F) %>% 
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive")) %>% 
#   row_spec(0, background = "orange")
```
<!-- ### <span style="color:green">High FC list (above 3.7)</span> -->
```{r, warning = F, message = F, echo = F, include = F}
# all_table[all_table$log2FoldChange>3.7,] %>% 
#   select(-links) %>% 
#   kable() %>% 
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed", "responsive"))
```
--------------------------------------------------------------------
```{r, warning = F, message = F, echo = F}
# all_table %>% arrange(average.vesicles)
```
```{r}
combined %>% filter(gene == "fusA")


```

```{r, warning = F, message = F, echo = F}
transferredRNAs = read.csv("C:/Users/rotemh.WISMAIN/Documents/R/rnamotive/simplex.csv")
# high_prev_res = prev_results %>% filter(count > 2) %>% cbind(c("ssrA", "rnpB", "rpoA", "pgk", "cysJ", "sgbU", "cp_Qin_Kim", "atpA", "metL", "lacZ", "yqeB","bdcR", "fusA", "cp_e14", "cp_rac", "hybO", "ftsK", "glnD", "aceA", "potB", "preA"))
high_prev_res = transferredRNAs %>% arrange(desc(count)) %>% slice(1:200)

colnames(high_prev_res)[2] = "wp"
together = merge(high_prev_res, combined)
intersect(high_prev_res$description, filtered$description)

cor(log(together$count), log(together$average.vesicles), method = "pearson")

cor(log(together$average.ecoli), log(together$count), method = "pearson")

# plot(together$gene, y = log(together$average.vesicles), type = "p", ylim = c(0,25))
# par(new=TRUE)
# points(x = together$gene, y = together$count, col = "red")
p =  ggplot(together, aes(x = reorder(gene, -average.vesicles))) +
  geom_col(    aes(y = count), shape = '1', size = 4, fill = '#0066DD') +
  geom_point(  aes(y = log(average.vesicles))
             , shape = 19, size = 4, color = '#F88379') +
  scale_y_log10(sec.axis = sec_axis(~2.71828182846**.
                                    , name = "vesicles reads\n (log scale)")
                , limits = c(1,30), breaks = c(10,20,30), expand = c(0.01, 0)) + 
  labs(y = "acceptor reads\n (log scale)", x = "gene") +
  theme_classic() +
  theme(  text = element_text(size=20, family = "Alef")
        , axis.text.x = element_text(angle = -45))

# p$theme$text$family = "alef"
p$theme$axis.text.y.right$colour = '#F88379'
p$theme$axis.text.y.right$family = "Alef"
p$theme$axis.title.y.right$family = "Alef"
p$theme$axis.title.y.right$colour = '#F88379'

p$theme$axis.title.y$colour = '#0066DD'
p$theme$axis.text.y$colour = '#0066DD'

p

write.csv(all_table %>% select(average.vesicles, average.bacteria, log2FoldChange, description), "data/simplex/average.expr.csv")
```
```{r}
all_table %>% arrange(desc(baseMean)) %>% select(description)
```
