send.que = function(cmd, memory=2000, q = "new-short", output, error, name = strsplit(cmd, "\\s+")[[1]][1]){
	#ifelse(missing(cmd), print("You must supply command for sending"))
	#ifelse(missing(memory), memory = 2000)
	#ifelse(missing(q), q = "new-short")
	#ifelse(missing(output), print("Please enter output file")
	#ifelse(missing(name), name = "something")
	all = paste0("bsub -q ", q, " -R 'rusage[mem=", memory, "]' -o ", output, " -J ", name," -e ", error, " ", cmd)
	system(all)
}