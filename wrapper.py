# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a wrapping script for the alignment and counting script.
It runs over each sample and:
    1) Aligns it against the relevant genome.
    2) Counts how many times each gene was observed.

IMPORTANT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    1) Before running the script on WEXAC, load the neede modules by:
        module load bwa samtools anaconda/5.2.0/python/3.6
    2) Make sure HTSeq is installed in your LOCAL conda environment.
       See how to create and activate a local environment in conda docs.
       Install HTSeq by typing the following command:
        conda install htseq -c bioconda
    3) Make sure that the samples \ genomes paths' in both wapper.py and align_and_count.py are correct!
    
"""

from pathlib import Path
import pandas as pd
import subprocess

home_path = Path('/home/labs/kolodkin/rotemh/HRT_project/')
samples_path = home_path / '181129_D00257_0327_BCCHWMANXX' / 'fastq'
metadata_file = home_path / 'metadata.csv'
metadata_df = pd.read_csv(metadata_file, index_col=0)


files = samples_path.glob('**/*.fastq.gz')
files = list(files)

for f in files:
    sample = f.name.split('_S')[0]
    genome = metadata_df['Genome'].at[sample].split(' ')[-1]
    genome = 'e_coli' if genome == 'coli' else genome
    
    cmd = 'bsub -q new-interactive "python align_and_count.py {} {}"'.format(f, genome)
    subprocess.run(cmd, shell=True)
    
f = files[4]    
sample = f.name.split('_S')[0]
genome = metadata_df['Genome'].at[sample].split(' ')[-1]
genome = 'e_coli' if genome == 'coli' else genome

cmd = 'bsub -q new-interactive "python align_and_count.py {} {}"'.format(f, genome)
subprocess.run(cmd, shell=True)
