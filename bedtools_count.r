# set gtf file location
gtfFile = "/home/labs/kolodkin/Collaboration/genome/NCIB_3610_merged_annotation_ssra.gtf"

# check input integrity
args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} else if (length(args)==1) {
  # default output file
  args[2] = "out.txt"
}

df = read.table(args[1], header=FALSE)
#print(df)

system("module add bedtools")
dir.create(paste0(getwd(), "/7_test"))

que = "new-short"
memory = "'rusage[mem=1000]'"
precommand = "bedtools coverage -counts -sorted -a "

for(sample in df[,1]){
  #print(sample)
  bamfile = paste0(getwd(), "/3_align/", sample, "_sorted.bam")
  output = paste0(getwd(), "/7_test/", sample, "_count.gtf")
  errorfile = paste0(getwd(), "/7_test/", sample, ".err")
  report = paste0(getwd(), "/7_test/", sample, ".log")
  cmd = paste0(precommand, gtfFile, " -b ", bamfile, " > ", output)
  print(cmd)
  system(cmd)
  
  # bedtools coverage -counts  -sorted -a /home/labs/kolodkin/Collaboration/genome/NCIB_3610_merged_annotation.1.gtf -b 3_align/B2_sorted.bam > output.txt
  
  
  # administration = paste0("bsub -q ", que, " -R ", memory, " -o ", report, " -e ", errorfile, " -J ", sample, " '", cmd,"'")
  # print(administration)
  # system(administration)
}
