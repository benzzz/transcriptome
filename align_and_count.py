#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 19 15:36:47 2018

@author: omera
"""

from sys import argv
import subprocess
from pathlib import Path

sample_file = Path(argv[1])
genome = argv[2]
genome_file = Path('/home/labs/kolodkin/rotemh/HRT_project/Genomes') / (genome + '.fasta')
gff_file = Path('/home/labs/kolodkin/rotemh/HRT_project/Genomes') / (genome + '.gff3')
log_file = sample_file.parent / 'log.txt'


cmd2 = "bwa aln {gen} {fastq} | bwa samse {gen} - {fastq} | samtools sort -n \
| htseq-count -f bam --stranded=no --type=CDS --mode=union -i ID \
--additional-attr=gene --samout={out_path}/out.sam - \
{gff} > {out_path}/counts.txt".format(gen=genome_file, fastq=sample_file, gff=gff_file, out_path=sample_file.parent)

proc = subprocess.run(cmd2, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

log_file.write_text(proc.stdout.decode())
log_file.write_text(proc.stderr.decode())
